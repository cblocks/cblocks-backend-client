**Beschreibung**

(Kurze Zusammenfassung des Bugs)

**Schritte zur Reproduktion**

1.
2.
3.

**Was ist das Verhalten des Bugs?**

(Was tatsächlich passiert)

**Was ist das erwartete korrekte Verhalten?**

(Was eigentlich passieren sollte)

**Relevante Logs und/oder Screenshots**

(Falls vorhanden relevante logs - benutze code blocks (```) um Konsolen Output zu formatieren)

**Mögliche Fixes**

(Falls möglich verlinke Code der mutmasslich für das Problem verantwortlich ist)

/label ~Bug

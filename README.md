# cblocks-backend-client
[![pipeline status](https://gitlab.com/cblocks/cblocks-backend-client/badges/master/pipeline.svg)](https://gitlab.com/cblocks/cblocks-backend-client/commits/master)
[![coverage report](https://gitlab.com/cblocks/cblocks-backend-client/badges/master/coverage.svg)](https://gitlab.com/cblocks/cblocks-backend-client/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/35790b48aa164676af7daf0cbbcba01c)](https://www.codacy.com/app/simonbreiter/cblocks-backend-client?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=cblocks/cblocks-backend-client&amp;utm_campaign=Badge_Grade)
[![Known Vulnerabilities](https://snyk.io/test/github/cblocks/cblocks-backend-client/badge.svg)](https://snyk.io/test/github/cblocks/cblocks-backend-client)
[![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

To contribute to this project, please read our [guidelines](./CONTRIBUTING.md) first.

module.exports = {
  moduleNameMapper: {
    'src/stories/([^\\.]*)$': '<rootDir>/src/stories/$1.vue'
  },
  moduleFileExtensions: ['js', 'vue'],
  transform: {
    '^.+\\.js$': '<rootDir>/node_modules/babel-jest',
    '.*\\.(vue)$': '<rootDir>/node_modules/vue-jest'
  },
  collectCoverageFrom: [
    '**/*.{js,jsx,vue}',
    '!**/node_modules/**',
    '!**/vendor/**',
    '!**/*.config.js',
    '!**/coverage/**',
    '!**/dist/**',
    '!**/test/**',
    'src/app.js',
    //'!**/login.vue',
    //'!**/account.vue',
    '!**/graphql/query.js',
    '!**/index.esm.js',
    '!**/store.js',
    '!**/app.js',
    '!**/server-production.js',
    '!**/server.js',
    '!**/router.js',
    '!**/posts.vue',
    '!**/post.vue',
    '!**/field.vue'
    //'!**/edit-type.vue',
  ]
}

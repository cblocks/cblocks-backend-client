const express = require('express')
const path = require('path')
const app = express()

app.use(express.static(__dirname))

app.get('/', function (req, res) {
  res.sendFile(path.join(__dirname + '/static/index-production.html'))
})

app.get('/app.js', function (req, res) {
  res.sendFile(path.join(__dirname + '/app.js'))
})

const port = process.env.PORT || 8080
module.exports = app.listen(port, () => {
  // console.log(`Server listening on http://localhost:${port}, Ctrl+C to stop`)
})

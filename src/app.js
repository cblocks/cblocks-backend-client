import Vue from 'vue'
import app from './app.vue'
import { apolloProvider } from './graphql/query'

import store from './store.js'
import router from './router.js'

store.dispatch('fetchTypes')
//store.dispatch('getContent')

new Vue({
  el: '#app',
  data: {
    loading: 0
  },
  apolloProvider,
  router,
  store,
  render: h => h(app)
})

import Vue from 'vue'
import VueApollo from 'vue-apollo'
import ApolloClient from 'apollo-client'
import gql from 'graphql-tag'
import { createHttpLink } from 'apollo-link-http'
import { setContext } from 'apollo-link-context'
import { InMemoryCache } from 'apollo-cache-inmemory'
import store from '../store'

Vue.use(VueApollo)

const httpLink = createHttpLink({
  uri: 'https://api.cblocks.simonbreiter.com/graphql'
})

const authLink = setContext((_, { headers }) => {
  const token = store.state.authToken
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : ''
    }
  }
})

// Cache implementation
const cache = new InMemoryCache()

const defaultOptions = {
  watchQuery: {
    fetchPolicy: 'network-only',
    errorPolicy: 'ignore'
  },
  query: {
    fetchPolicy: 'network-only',
    errorPolicy: 'all'
  }
}

// Create the apollo client
const apolloClient = new ApolloClient({
  link: authLink.concat(httpLink),
  cache,
  defaultOptions
})

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient
})

const getGraphql = (query, accessor) => async variables => {
  const res = await apolloClient.query({
    query,
    variables
  })
  return res.data[accessor]
}

const setGraphql = (mutation, accessor) => async input => {
  const res = await apolloClient.mutate({
    mutation,
    variables: { input }
  })
  return res.data[accessor]
}

const routesQuery = gql`
  query {
    routeList {
      name
      query
    }
  }
`

const routeQuery = gql`
  query($name: String!) {
    route(name: $name) {
      name
      query
    }
  }
`

const createRouteQuery = gql`
  mutation($input: createRouteInput!) {
    createRoute(input: $input) {
      name
      query
    }
  }
`

const updateRouteQuery = gql`
  mutation($input: updateRouteInput!) {
    updateType(input: $input) {
      name
      query
    }
  }
`

const deleteRouteQuery = gql`
  mutation($input: deleteRouteInput!) {
    deleteType(input: $input) {
      name
      query
    }
  }
`

const typesQuery = gql`
  query {
    typeList {
      id
      name
      label
      description
      fields {
        id
        name
        label
        description
        index
        shortform
        fieldtype {
          id
          name
          label
          description
        }
        target {
          id
        }
      }
    }
  }
`

const typeQuery = gql`
  query($id: Int!) {
    type(id: $id) {
      id
      name
      label
      description
      fields {
        id
        name
        label
        description
        index
        shortform
        fieldtype {
          id
          name
          label
          description
        }
        target {
          id
        }
      }
    }
  }
`

const createTypeQuery = gql`
  mutation($input: createTypeInput!) {
    createType(input: $input) {
      id
      name
      label
      description
    }
  }
`

const updateTypeQuery = gql`
  mutation($input: updateTypeInput!) {
    updateType(input: $input) {
      id
      name
      label
      description
    }
  }
`

const deleteTypeQuery = gql`
  mutation($input: deleteTypeInput!) {
    deleteType(input: $input) {
      id
      name
      label
      description
    }
  }
`

const fieldtypesQuery = gql`
  query {
    fieldtypeList {
      id
      name
      label
      description
    }
  }
`

const fieldtypeQuery = gql`
  query($id: Int!) {
    fieldtype(id: $id) {
      id
      name
      label
      description
    }
  }
`

const createFieldtypeQuery = gql`
  mutation($input: createFieldtypeInput!) {
    createFieldtype(input: $input) {
      id
      name
      label
      description
    }
  }
`

const updateFieldtypeQuery = gql`
  mutation($input: updateFieldtypeInput!) {
    updateFieldtype(input: $input) {
      id
      name
      label
      description
    }
  }
`

const deleteFieldtypeQuery = gql`
  mutation($input: deleteFieldtypeInput!) {
    deleteFieldtype(input: $input) {
      id
      name
      label
      description
    }
  }
`

const fieldsQuery = gql`
  query {
    fieldList {
      id
      type {
        id
      }
      name
      label
      description
      index
      shortform
      fieldtype {
        id
      }
      target {
        id
      }
    }
  }
`

const fieldQuery = gql`
  query($id: Int!) {
    field(id: $id) {
      id
      name
      label
      description
      index
      shortform
      fieldtype {
        id
        name
        description
      }
      target {
        id
      }
    }
  }
`

const createFieldQuery = gql`
  mutation($input: createFieldInput!) {
    createField(input: $input) {
      id
      name
      label
      description
      type
      fieldtype
      target
      index
      shortform
    }
  }
`

const updateFieldQuery = gql`
  mutation($input: updateFieldInput!) {
    updateField(input: $input) {
      id
      name
      label
      description
      type
      fieldtype
      target
      index
      shortform
    }
  }
`

const deleteFieldQuery = gql`
  mutation($input: deleteFieldInput!) {
    deleteField(input: $input) {
      id
      name
      label
      description
      type
      fieldtype
      target
      index
      shortform
    }
  }
`

const typeDefinition = gql`
  query($id: Int!) {
    type(id: $id) {
      name
      label
      fields {
        name
        fieldtype {
          id
        }
      }
    }
  }
`
export const getGeneratedTypeFns = async id => {
  const getTypeDefinition = getGraphql(typeDefinition)
  const t = await type.getById({ id })
  return {
    getAll: async () => {
      const res = await apolloClient.query({
        query: gql`query{
          ${t.name}List{
            id
            ${t.fields
              .map(f =>
                f.fieldtype.id == 1 || f.fieldtype.id == 2
                  ? `${f.name}{id}`
                  : f.name
              )
              .join('\n')}
          }
        }`
      })
      return res.data[`${t.name}List`]
    },
    getById: async id => {
      const res = await apolloClient.query({
        query: gql`
        query($id: Int!){
          ${t.name}(id:$id){
            id
            ${t.fields
              .map(f =>
                f.fieldtype.id == 1 || f.fieldtype.id == 2
                  ? `${f.name}{id}`
                  : f.name
              )
              .join('\n')}
          }
        }`,
        variables: { id }
      })
      return res.data[t.name]
    },
    create: async input => {
      const res = await apolloClient.mutate({
        mutation: gql`
        mutation($input: create${t.name}Input!){
          create${t.name}(input:$input){
            id
            ${t.fields.map(f => f.name).join('\n')}
          }
        }`,
        variables: { input }
      })
      return res.data[`create${t.name}`]
    },
    update: async input => {
      const res = await apolloClient.mutate({
        mutation: gql`
        mutation($input: update${t.name}Input!){
          update${t.name}(input:$input){
            id
            ${t.fields.map(f => f.name).join('\n')}
          }
        }`,
        variables: { input }
      })
      return res.data[`update${t.name}`]
    },
    delete: async id => {
      const res = await apolloClient.mutate({
        mutation: gql`
        mutation($input: delete${t.name}Input!){
          delete${t.name}(input:$input){id}
        }`,
        variables: { input: { id } }
      })
      return res.data[`delete${t.name}`]
    }
  }
}

/*
fetch(`http://localhost:3000/graphql`,{
  method:"POST", 
  body: JSON.stringify({ "query": '{ typeList { id } }' }),
  headers: {
    "Content-Type": "application/json"
  },
}).then(res=>console.log(JSON.stringify(res,null,2)))
*/
/*
export const route = {
  getAll: getGraphql(routesQuery, 'routeList'),
  getById: getGraphql(routeQuery, 'route'),
  create: setGraphql(createRouteQuery, 'createRoute'),
  update: setGraphql(updateRouteQuery, 'updateRoute'),
  delete: setGraphql(deleteRouteQuery, 'deleteRoute')
}
*/
export const type = {
  getAll: getGraphql(typesQuery, 'typeList'),
  getById: getGraphql(typeQuery, 'type'),
  create: setGraphql(createTypeQuery, 'createType'),
  update: setGraphql(updateTypeQuery, 'updateType'),
  delete: setGraphql(deleteTypeQuery, 'deleteType')
}

export const fieldtype = {
  getAll: getGraphql(fieldtypesQuery, 'fieldtypeList'),
  getById: getGraphql(fieldtypeQuery, 'fieldtype'),
  create: setGraphql(createFieldtypeQuery, 'createFieldtype'),
  update: setGraphql(updateFieldtypeQuery, 'updateFieldtype'),
  delete: setGraphql(deleteFieldtypeQuery, 'deleteFieldtype')
}

export const field = {
  getAll: getGraphql(fieldsQuery, 'fieldList'),
  getById: getGraphql(fieldQuery, 'field'),
  create: setGraphql(createFieldQuery, 'createField'),
  update: setGraphql(updateFieldQuery, 'updateField'),
  delete: setGraphql(deleteFieldQuery, 'deleteField')
}

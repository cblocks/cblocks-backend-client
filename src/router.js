import Vue from 'vue'
import VueRouter from 'vue-router'
import { sync } from 'vuex-enhanced-router-sync'

import contentTypes from './components/pages/content-types.vue'
import editType from './components/pages/edit-type.vue'
import dashboard from './components/pages/dashboard.vue'
import account from './components/pages/account.vue'
import login from './components/pages/login.vue'
import posts from './components/pages/posts.vue'
import post from './components/pages/post.vue'
import store from './store.js'

Vue.use(VueRouter)
const routes = [
  { path: '/', component: dashboard },
  { path: '/dashboard', component: dashboard },
  { path: '/types/', component: contentTypes },
  { path: '/types/:type', component: editType },
  { path: '/posts/:type', component: posts },
  { path: '/posts/:type/:post', component: post },
  { path: '/account', component: account },
  { path: '/login', component: login }
]
const router = new VueRouter({
  routes: routes
})

router.beforeEach((to, from, next) => {
  console.log(to, from)
  if (to.path !== '/login' && !store.getters.getAuthToken) {
    next('/login')
  } else {
    next()
  }
})

/*
 * Keep route state in sync inside our vuex store
 * https://github.com/vuejs/vuex-router-sync
 */
const unsync = sync(store, router)
//unsync() // Unsyncs store from router

export default router

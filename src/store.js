import Vue from 'vue'
import Vuex from 'vuex'

import {
  type as gqltype,
  fieldtype as gqlfieldtype,
  field as gqlfield,
  getGeneratedTypeFns
} from './graphql/query'

export const postParamsReducer = post => (acc, field) =>
  field.fieldtype.id == 1
    ? { ...acc, [field.name]: post[field.name] ? post[field.name].id : null }
    : field.fieldtype.id == 2
    ? { ...acc, [field.name]: post[field.name].map(p => p.id) }
    : { ...acc, [field.name]: post[field.name] }

Vue.use(Vuex)

const store = new Vuex.Store({
  strict: true,
  state: {
    menuActive: false,
    dataLoaded: false,
    contentTypes: {},
    postFunctions: {},
    posts: {},
    editedType: {
      name: 'New',
      description: 'This is a new type',
      fields: []
    },
    editedPost: {},
    authToken: null
  },
  mutations: {
    openMenu (state) {
      Vue.set(state, 'menuActive', true)
    },
    closeMenu (state) {
      Vue.set(state, 'menuActive', false)
    },
    dataLoaded (state, loaded) {
      Vue.set(state, 'dataLoaded', loaded)
    },
    setContentTypes (state, contentTypes) {
      Vue.set(state, 'contentTypes', contentTypes)
    },
    setPosts (state, { id, posts }) {
      Vue.set(state.posts, id, posts)
    },
    setPost (state, { typeId, post }) {
      Vue.set(state.posts[typeId], post.id, post)
    },
    removePost (state, { typeId, post }) {
      Vue.delete(state.posts[typeId], post.id)
    },
    setEditedType (state, type) {
      Vue.set(state, 'editedType', JSON.parse(JSON.stringify(type)))
    },
    resetEditedType (state) {
      Vue.set(state, 'editedType', {
        label: 'new',
        name: 'New',
        description: 'This is a new type',
        fields: []
      })
    },
    setEditedPost (state, post) {
      const editedPost = JSON.parse(JSON.stringify(post))
      delete editedPost.__typename
      Vue.set(state, 'editedPost', editedPost)
    },
    setPostProperty (state, { key, value }) {
      Vue.set(state.editedPost, key, value)
    },
    resetPost (state, typeId) {
      const type = state.contentTypes[typeId]
      if (type) {
        const postSkeleton = type.fields.reduce(
          (acc, field) =>
            field.fieldtype.id == 1
              ? { ...acc, [field.name]: undefined }
              : field.fieldtype.id == 2
              ? { ...acc, [field.name]: [] }
              : { ...acc, [field.name]: '' },
          {}
        )
        Vue.set(state, 'editedPost', postSkeleton)
      }
    },
    setFields (state, fields) {
      Vue.set(state.editedType, 'fields', fields)
    },
    addField (state) {
      Vue.set(state.editedType, 'fields', [
        ...state.editedType.fields,
        {
          index: 0,
          shortform: false,
          name: 'NewField',
          label: 'New Field',
          description: 'This is a new field',
          fieldtype: { id: 3 },
          target: { id: -1 }
        }
      ])
    },
    setFieldProperty (state, { index, key, value }) {
      Vue.set(state.editedType.fields[index], key, value)
    },
    deleteType (state, type) {
      Vue.delete(state.contentTypes, type.id)
    },
    setType (state, type) {
      Vue.set(state.contentTypes, type.id, type)
    },
    setTypeProperty (state, { key, value }) {
      Vue.set(state.editedType, key, value)
    },
    setFunctions (state, { id, functions }) {
      Vue.set(state.postFunctions, id, functions)
    },
    setAuthToken (state, token) {
      state.authToken = token
    }
  },
  actions: {
    async fetchTypes (context) {
      gqltype.getAll().then(res => {
        const contentTypes = res.reduce(
          (acc, type) => ({ ...acc, [type.id]: type }),
          {}
        )
        context.commit('setContentTypes', contentTypes)
        context.commit('dataLoaded', true)
        if (
          context.state.route.params.type &&
          context.state.route.params.type != 'new'
        ) {
          context.commit(
            'setEditedType',
            context.getters.getTypeById(context.state.route.params.type)
          )
        }
      })
    },
    async fetchPostsByType (context, id) {
      if (context.getters.getPostsByTypeId(id)) {
        return
      }
      const functions = await getGeneratedTypeFns(id)
      const postArray = await functions.getAll()
      const posts = postArray.reduce(
        (acc, post) => ({ ...acc, [post.id]: post }),
        {}
      )
      context.commit('setFunctions', { id, functions })
      context.commit('setPosts', { id, posts })
    },
    async deleteType (context, type) {
      const deletedType = await gqltype.delete({
        id: type.id
      })
      context.commit('deleteType', deletedType)
    },
    async createField (context, { type, field }) {
      const {
        name,
        label,
        description,
        fieldtype,
        target,
        index,
        shortform
      } = field
      const fieldParams = {
        name,
        label,
        description,
        fieldtype: fieldtype.id,
        type: type.id,
        index,
        shortform
      }
      if (fieldtype.id == 1 || fieldtype.id == 2) {
        fieldParams.target = target.id
      }
      return await gqlfield.create(fieldParams)
    },
    async updateField (context, field) {
      const {
        id,
        name,
        label,
        description,
        fieldtype,
        index,
        shortform
      } = field
      const fieldParams = {
        id,
        name,
        label,
        description,
        fieldtype: fieldtype.id,
        index,
        shortform
      }
      return await gqlfield.update(fieldParams)
    },
    async deleteField (context, { field, type }) {
      const res = await gqlfield.delete({ id: field.id })
      const result = await gqltype.getById({ id: type.id })
      context.commit('setType', result)
      context.commit('setEditedType', result)
    },
    async createType (context, type) {
      const { name, label, description } = type
      const queryType = { name, label, description }
      const createdType = await gqltype.create(queryType)
      const fields = await type.fields.reduce(async (acc, field) => {
        const newacc = await acc
        const createdField = await context.dispatch('createField', {
          field,
          type: createdType
        })
        return [...newacc, createdField]
      }, Promise.resolve([]))
      const result = await gqltype.getById({ id: createdType.id })
      context.commit('setType', result)
      context.commit('setEditedType', result)
    },
    async updateType (context, type) {
      const { id, name, label, description } = type
      const queryType = { id, name, label, description }
      const updatedType = await gqltype.update(queryType)
      const fields = await type.fields.reduce(async (acc, field) => {
        const newacc = await acc
        const updatedField = field.id
          ? await context.dispatch('updateField', field)
          : await context.dispatch('createField', { type, field })
        return [...newacc, updatedField]
      }, Promise.resolve([]))
      const result = await gqltype.getById({ id: type.id })
      context.commit('setType', result)
      context.commit('setEditedType', result)
    },
    async createPost (context, { typeId, post }) {
      const postParams = context.state.contentTypes[typeId].fields.reduce(
        postParamsReducer(post),
        {}
      )
      const result = await context.state.postFunctions[typeId].create(
        postParams
      )
      const newPost = await context.state.postFunctions[typeId].getById(
        result.id
      )
      context.commit('setPost', { typeId, post: newPost })
    },
    async updatePost (context, { typeId, post }) {
      const postParams = JSON.parse(
        JSON.stringify(context.getters.getContentTypes[typeId].fields)
      ).reduce(postParamsReducer(post), { id: post.id })
      const result = await context.state.postFunctions[typeId].update(
        postParams
      )
      const newPost = await context.state.postFunctions[typeId].getById(
        result.id
      )
      context.commit('setPost', { typeId, post: newPost })
    },
    async deletePost (context, { typeId, post }) {
      const { id } = post
      const result = await context.state.postFunctions[typeId].delete(id)
      context.commit('removePost', { typeId, post })
    },
    async authenticate (context, { username, password }) {
      if (username && password) {
        const data = new FormData()
        data.append('username', username)
        data.append('password', password)
        const response = await fetch(
          'https://api.cblocks.simonbreiter.com/auth/login',
          {
            method: 'post',
            headers: {
              Accept: 'application/json, text/plain, */*',
              'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: new URLSearchParams(data)
          }
        ).then(res => res.json())
        if (response && response.token) {
          context.commit('setAuthToken', response.token)
          return response.token
        }
      }
      return false
    }
  },
  getters: {
    route: state => state.route,
    dataLoaded: state => () => state.dataLoaded,
    getContentTypes: state => state.contentTypes,
    getEditedType: state => state.editedType,
    getEditedFields: state => state.editedType.fields,
    getTypeById: state => id => state.contentTypes[id],
    getPostById: state => (typeId, postId) => state.posts[typeId][postId],
    getPostsByTypeId: state => id => state.posts[id],
    getEditedPost: state => state.editedPost,
    getAuthToken: state => state.authToken
  }
})

export default store

// exports.default = store

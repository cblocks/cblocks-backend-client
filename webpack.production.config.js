const path = require('path')
const webpack = require('webpack')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CopyPlugin = require('copy-webpack-plugin')
//const VueSSRClientPlugin = require('vue-server-renderer/client-plugin')

module.exports = {
  mode: 'production',
  target: 'web',
  entry: './src/app.js',

  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'app.js'
    //libraryTarget: 'commonjs2'
  },

  module: {
    rules: [
      { test: /\.js$/, exclude: /node_modules/, use: ['babel-loader'] },
      { test: /\.vue$/, use: ['vue-loader'] },
      { test: /\.css$/, use: ['vue-style-loader', 'css-loader'] },
      {
        test: /\.s[a|c]ss$/,
        use: ['vue-style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },

  plugins: [
    new VueLoaderPlugin(),
    new webpack.NoEmitOnErrorsPlugin(),
    new CopyPlugin([
      './src/production-server.js',
      { from: './static', to: 'static' }
    ])
    //new VueSSRClientPlugin()
  ]
}

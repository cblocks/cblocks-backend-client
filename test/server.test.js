const request = require('supertest')
const server = require('../src/production-server.js')

describe('Test the root path', () => {
  test('Production server is running', async () => {
    const response = await request(server).get('/#/')
    expect(response.statusCode).toBe(404)
    server.close()
  })
})

// https://vue-test-utils.vuejs.org/api/
import { createLocalVue, shallowMount } from '@vue/test-utils'
import app from '../src/app.vue'
import VueRouter from 'vue-router'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Button Component', () => {
  test('is a Vue instance', () => {
    const comp = shallowMount(app, { localVue })
    expect(comp.isVueInstance()).toBeTruthy()
  })
})

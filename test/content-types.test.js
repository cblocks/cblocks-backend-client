import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import contentTypes from '../src/components/pages/content-types.vue'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Content Types Page Component', () => {
  let actions
  let store
  let getters

  beforeEach(() => {
    actions = {
      actionClick: jest.fn(),
      actionInput: jest.fn()
    }
    getters = {
      getContentTypes: function () {}
    }
    store = new Vuex.Store({
      actions,
      getters
    })
  })

  test('is a Vue instance', () => {
    const wrapper = shallowMount(contentTypes, { store, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})

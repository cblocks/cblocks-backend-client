// https://vue-test-utils.vuejs.org/api/
import { mount } from '@vue/test-utils'
import btn from '../src/components/btn.vue'

describe('Button Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(btn)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('Default slot displays button text', () => {
    let buttonText = 'Please click me'
    const comp = mount(btn, {
      slots: {
        default: [buttonText]
      }
    })
    expect(comp.contains('span.button')).toBe(true)
    expect(comp.text()).toContain(buttonText)
  })
})

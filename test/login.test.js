import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import login from '../src/components/pages/login.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Content Types Page Component', () => {
  let actions
  let store

  beforeEach(() => {
    actions = {
      actionClick: jest.fn(),
      actionInput: jest.fn()
    }
    store = new Vuex.Store({
      actions
    })
  })
  test('is a Vue instance', () => {
    const wrapper = shallowMount(login, { store, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})

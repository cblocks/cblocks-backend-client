// https://vue-test-utils.vuejs.org/api/
import { mount } from '@vue/test-utils'
import userpanel from '../src/components/userpanel.vue'

describe('Userpanel Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(userpanel)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('toggle() displays pulldown', () => {
    const comp = mount(userpanel)
    let wrapperClass = '.dd-menu'
    expect(comp.contains(wrapperClass)).toBe(false)
    comp.vm.toggle()
    expect(comp.contains(wrapperClass)).toBe(true)
  })

  test('close() hides pulldown', () => {
    const comp = mount(userpanel)
    let wrapperClass = '.dd-menu'
    expect(comp.contains(wrapperClass)).toBe(false)
    comp.vm.toggle()
    expect(comp.contains(wrapperClass)).toBe(true)
    comp.vm.close()
    expect(comp.contains(wrapperClass)).toBe(false)
  })

  test('displays users name', () => {
    const comp = mount(userpanel)
    comp.setData({
      user: {
        userName: 'max.muster',
        firstName: 'Max',
        lastName: 'Muster',
        image:
          'https://ownbit.net/wp-content/uploads/2016/08/michael-eichmann.jpg'
      }
    })
    expect(comp.text()).toMatch(/(.)*(Max Muster)(.)*/)
  })

  test('click on panel toggles dropdown', () => {
    const comp = mount(userpanel)
    let wrapperClass = '.dd-menu'
    comp.find('.user-name').trigger('click')
    expect(comp.contains(wrapperClass)).toBe(true)
  })
})

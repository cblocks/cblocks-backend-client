// https://vue-test-utils.vuejs.org/api/
import { shallowMount } from '@vue/test-utils'
import router from '../src/router.js'
import fetch from 'unfetch'
import { createHttpLink } from 'apollo-link-http'

describe('Router Config', () => {
  test('has main pages', () => {
    let routes = router.options.routes
    console.log(routes)

    expect(routes.length).toBeGreaterThan(0)
  })
})

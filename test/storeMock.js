import { createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

let actions = {
  actionClick: jest.fn(),
  actionInput: jest.fn()
}

let getters = {
  clicks: () => 2,
  inputValue: () => 'input',
  getEditedFields: () => [],
  getTypeById: () => {}
}

let storeMock = new Vuex.Store({
  actions: actions,
  getters: getters
})

export default storeMock

import { mount } from '@vue/test-utils'
import ContentField from '../src/components/ContentField.vue'

describe('ContentField Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(ContentField)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  global.console = {
    warn: jest.fn(),
    log: jest.fn()
  }

  test('copyField() logs to console', () => {
    const comp = mount(ContentField)
    comp.vm.copyField()
    expect(global.console.log).toHaveBeenCalledWith('should copy now')
  })

  test('deleteField() logs to console', () => {
    const comp = mount(ContentField)
    comp.vm.deleteField()
    expect(global.console.log).toHaveBeenCalledWith('should delete now')
  })

  test('should contain 2 buttons', () => {
    const comp = mount(ContentField)
    expect(comp.contains('button')).toBe(true)
  })
})

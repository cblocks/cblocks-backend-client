// https://vue-test-utils.vuejs.org/api/
import { shallowMount, mount } from '@vue/test-utils'
import field from '../src/components/field.vue'
// import storeMock from './storeMock.js'
import Vuex from 'vuex'
import { createLocalVue } from '@vue/test-utils'

const localVue = createLocalVue()
localVue.use(Vuex)
const storeMock = jest.genMockFromModule('../src/store.js')

describe('Field Component', () => {
  // let storeMock = new Vuex.Store({
  //   actions: {
  //     actionClick: jest.fn(),
  //     actionInput: jest.fn()
  //   },
  //   getters: {
  //     route: state => {},
  //     dataLoaded: state => () => {},
  //     getContentTypes: state => {},
  //     getEditedType: state => {},
  //     getEditedFields: state => {},
  //     getTypeById: state => {},
  //     getPostById: state => (typeId, postId) => {},
  //     getPostsByTypeId: state => id => {},
  //     getEditedPost: state => {}
  //   },
  // })
  // test('is a Vue instance', () => {
  //   console.log(storeMock)
  //   let comp = shallowMount(field, {storeMock, localVue})
  //   expect(comp.isVueInstance()).toBeTruthy()
  // })
  // test('Opens edit form', () => {
  //   let comp = mount(field)
  //   let editFormSelector = '.edit-form'
  //   expect(comp.contains(editFormSelect)).toBe(false)
  //   comp.vm.edit()
  //   expect(comp.contains(editFormSelect)).toBe(tue)
  // })
  // test('Closes edit form', () => {
  //
  // })
  //
  // test('Toggles edit form', () => {
  //   const comp = mount(field)
  //   let wrapperClass = '.win'
  //   expect(comp.contains(wrapperClass)).toBe(false)
  //   comp.vm.toggleEdit()
  //   expect(comp.contains(wrapperClass)).toBe(true)
  // })
})

import { mount } from '@vue/test-utils'
import ContentField from '../src/components/ContentField.vue'

describe('ContentField Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(ContentField)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('should contain 2 buttons', () => {
    const comp = mount(ContentField)
    expect(comp.contains('button')).toBe(true)
  })
})

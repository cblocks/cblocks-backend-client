import { mount } from '@vue/test-utils'
import account from '../src/components/pages/account.vue'

describe('Account Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(account)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('should contain header and div', () => {
    const comp = mount(account)
    expect(comp.contains('h2')).toBe(true)
    expect(comp.contains('div')).toBe(true)
  })
})

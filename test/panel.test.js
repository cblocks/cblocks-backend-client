import { createLocalVue, mount } from '@vue/test-utils'
import sidebar from '../src/components/sidebar.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Panel Component', () => {
  let store

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        fields: {}
      }
    })
  })

  test('is a Vue instance', () => {
    const comp = mount(sidebar, { store, localVue })
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('should contain logo', () => {
    const comp = mount(sidebar, { store, localVue })
    expect(comp.contains('img')).toBe(true)
  })
})

import { createLocalVue, shallowMount } from '@vue/test-utils'
import Vuex from 'vuex'
import fieldsPanel from '../src/components/pages/fields-panel.vue'
import fields from '../src/components/field.vue'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Field Panel Page', () => {
  let actions
  let store

  beforeEach(() => {
    store = new Vuex.Store({
      modules: {
        fields: {
          getters: fields.getters
        }
      }
    })
  })
  test('is a Vue instance', () => {
    const wrapper = shallowMount(fieldsPanel, { store, localVue })
    expect(wrapper.isVueInstance()).toBeTruthy()
  })
})

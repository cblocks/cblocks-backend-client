// https://vue-test-utils.vuejs.org/api/
import { mount } from '@vue/test-utils'
import win from '../src/components/win.vue'

describe('Window Component', () => {
  test('is a Vue instance', () => {
    const comp = mount(win)
    expect(comp.isVueInstance()).toBeTruthy()
  })

  test('open() displays window', () => {
    const comp = mount(win)
    let wrapperClass = '.win'
    expect(comp.contains(wrapperClass)).toBe(false)
    comp.vm.open()
    expect(comp.contains(wrapperClass)).toBe(true)
  })

  test('close() removes window', () => {
    const comp = mount(win)
    let wrapperClass = '.win'
    expect(comp.contains(wrapperClass)).toBe(false)
    comp.vm.open()
    expect(comp.contains(wrapperClass)).toBe(true)
    comp.vm.close()
    expect(comp.contains(wrapperClass)).toBe(false)
  })
})
